# -*- coding: utf-8 -*-

import sys
import urllib
from cStringIO import StringIO
from time import time

from lxml import etree

direct = sys.argv[1] #'http://sushi-karate.ru/rolly'
elements = []

class HtmlCode(object):
    '''A class for grabbing code from url'''
    def __init__(self, url):
        self.url = url

    def grab(self):
        '''A function for grabbing html from url'''
        html = urllib.urlopen(self.url).read()
        parser = etree.HTMLParser()
        return etree.parse(StringIO(html), parser)

class Roll(object):
    '''A class for parsing grabbed code'''
    def __init__(self, path_name, path_price, code):
        self.code = code
        self.path_name = path_name
        self.path_price = path_price

    def make(self):
        '''A function for parse names and prices of positions in menu'''
        name = self.code.xpath(self.path_name)[0].encode('utf-8')
        price = self.code.xpath(self.path_price)[0].encode('utf-8')
        return str(price) + ',' + str(name)

def collect_elements(*args):
    '''Take xpaths to parser'''
    for e in args:
        x_name = '//div[@id="box"]/div[2]/table[2]/tbody/tr/td/ul/li[' + str(e) + ']/table/tbody/tr[1]/td/span/a/text()'
        x_price = '//div[@id="box"]/div[2]/table[2]/tbody/tr/td/ul/li[' + str(e) + ']/table/tbody/tr[5]/td/span/text()'
        position = Roll(x_name, x_price, html).make()
        elements.append(position)

start = time() #Start the timer
html = HtmlCode(direct).grab()
collect_elements(2, 3, 4, 7, 10, 11, 12, 13)
finish = time() #Stop timer

#Write data in file
with open('result.txt', 'w+') as file:
    for element in elements:
        file.write('%s \n' % element)
    file.write('\nTime: ' + str(finish - start))
